import React, { Component } from "react";
import { Redirect, NavLink } from "react-router-dom";
import SimpleReactValidator from "simple-react-validator";
import auth from "../auth";
import styled from 'styled-components';
import withConsumer from "../components/AppContext/withConsumer";
import { Button, Row, Col, Layout } from 'antd';
import Input from '../components/Input';
import logo1 from '../assets/images/happycodesite.png';
import Links from '../components/Links/index.jsx';



const { Content } = Layout;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      answer: "",
      isLoadingButton: false
    };

    this.validator = new SimpleReactValidator({
      messages: {
        default: "Campo obrigatório"
      }
    });
  }

  componentWillMount() {
    if (this.props.userAuth) {
      this.props.history.push("/pre-matriculas");
    }
  }

  componentDidMount() {
    document.title = "Login - Happy Code Aracaju";
  }

  handleFormChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
    this.setState({ answer: "" });
  };

  handleSubmit = async e => {
    try {
      e.preventDefault();

      this.setState({ isLoadingButton: true });

      this.setState({ answer: "" });

      if (!this.validator.allValid()) {
        this.setState({
          answer: [
            <i className="material-icons" key="">
              error_outline
            </i>,
            "Erros de validações nos campos."
          ]
        });
        this.setState({ isLoadingButton: false });
        this.validator.showMessages();
        this.forceUpdate();
        return;
      }

      await auth.login(this.state.email, this.state.password);
    } catch (error) {
      if (error.code === "auth/user-disabled") {
        this.setState({
          answer: [
            <i className="material-icons align-icon" key="">
              error_outline
            </i>,
            "Usuário desabilitado. Entre em contato com o administrador!"
          ]
        });
      } else {
        this.setState({
          answer: [
            <i className="material-icons align-icon" key="">
              error_outline
            </i>,
            "E-mail ou senha inválidos. Tente novamente!"
          ]
        });
      }
      this.setState({ isLoadingButton: false });
    }
  };

  render() {
    if (this.props.userAuth) {
      return <Redirect to="/pre-matriculas" />;
    }

    return (
      <div id="login-view">
        <div className="section">
          <StyledContent>
            <Row>
              <Col span={24}>

                <div>
                  
                  <form onSubmit={this.handleSubmit}>
                  <div class='logo' />
                    <div className="field">
                      <StyledInput
                        label="Login"
                        name="email"
                        type="email"
                        placeholder="e-mail"
                        value={this.state.email}
                        onChange={this.handleFormChange}
                      />
                      <span>
                        {this.validator.message(
                          "email",
                          this.state.email,
                          "required"
                        )}
                      </span>
                    </div>

                    <div className="field margin-input">
                      <StyledInput
                        label="Senha"
                        name="password"
                        type="password"
                        placeholder="senha"
                        value={this.state.password}
                        onChange={this.handleFormChange}
                      />
                      <span>
                        {this.validator.message(
                          "password",
                          this.state.password,
                          "required"
                        )}
                      </span>
                    </div>

                    <div className="answer margin-input">
                      {this.state.answer}
                    </div>

                    <div className="field margin-other btn">
                      <StyledButton
                        htmlType="submit"
                        type="primary"
                        loading={this.state.isLoadingButton}
                      >
                        Entrar
                      </StyledButton>
                    </div>
                    <div className="go-forgot">
                    <NavLink to="/forgot">Esqueceu sua senha?</NavLink>
                    </div>

                    {/* <div className="field margin-other forgot">
                      <NavLink to="/login/redefinir-senha">
                        Esqueceu sua senha?
                      </NavLink>
                    </div> */}
                  </form>
                </div>
              </Col>
            </Row>
          </StyledContent>
        </div>
      </div>
    );
  }
}

const StyledContent = styled(Content)`
  background: #d9fcdf;
  display: flex;
  height: 100vh;
  justify-content: center; 
  align-items: center;

  form {
    color: #895BF2;
    width: auto;
    height: 300px;
    background: #fff;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
    padding: 30px;
  }

  form label {
    font-style: italic;
    font-weight: bold
  }

  .srv-validation-message {
    color: red;
  }
  
  .logo{
      width: 50px;
      height: 50px;
      background-image: url(${logo1});
      background-repeat: no-repeat;
      position: relative;
      display: block !important;
      margin: auto;
  }
  .go-forgot{
      position: relative;
      text-align: center;
      display: block !important;
      margin: auto;
    }
`;

const StyledInput = styled(Input)`
  margin-bottom: 10px !important;
`;

const StyledButton = styled(Button)`
  display: block !important;
  /* margin: auto !important; */
  margin-top:10px !important;
  margin: 20px auto !important;
  background: #895BF2 !important;
  border-color: #895BF2 !important;
`;

export default withConsumer(Login);
