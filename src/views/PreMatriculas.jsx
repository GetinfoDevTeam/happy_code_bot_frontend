import React, { Component } from 'react';
import Axios from 'axios';
import { NavLink } from 'react-router-dom';
import { Button, Skeleton } from 'antd';
import { Layout, Menu, Icon, Breadcrumb } from 'antd';
import styled from 'styled-components';

import moment from "moment";
import "moment/locale/pt-br";
//scripts
import scripts from '../scripts';
//components
import Table from '../components/Table';
import Pagination from '../components/Pagination/index.jsx';
import logo1 from '../assets/images/happycodesite.png';




const { Header, Content, Footer } = Layout;

class PreMatriculas extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoadingTable: true,
      msgError: "",
      preMatriculaList: [],

      limit: 10,
      currentPage: 1,
      totalResults: 0,
    };
  }

  componentDidMount() {
    document.title = "Pré-matrículas"
    this.getPreMatriculas();
  }

  getPreMatriculas = async (offset = 0) => {
    Axios.get(`https://us-central1-bot-happy-code.cloudfunctions.net/api/pre-matricula?limit=${this.state.limit}&init=${offset}`)
      .then(res => {
        let result = res.data.result;
        let data = [];

        if (result.length > 0) {
          for (let i = 0; i < result.length; i++) {
            data.push({
              name: scripts.capitalizeFirstLetter(result[i].name),
              age: result[i].age,
              class: result[i].class,
              schedule: result[i].schedule,
              createdAt: moment(result[i].createdAt).format('l'),
              parentsName: scripts.capitalizeFirstLetter(result[i].parentsName),
              parentsContact: scripts.prettyPhone(result[i].parentsContact),
              parentsEmail: result[i].parentsEmail
            });
          }
        }

        this.setState({ preMatriculaList: data });
        this.setState({ totalResults: res.data.count });
      })
      .catch(err => {
        this.setState({ msgError: "Aconteceu algum erro ao realizar a busca dos cadastros. Tente novamente!" });
        console.log(err);
      })
      .then(() => {
        this.setState({ isLoadingTable: false });
      })
  }

  /**
   * 
   * INICIO - Paginação
   * 
   */

  goToPage = (currentPage) => {
    this.setState({ isLoadingTable: true });
    this.setState({ currentPage });
    const offset = (currentPage - 1) * this.state.limit;
    this.getPreMatriculas(offset);
    window.scrollTo(0, 0);
  };

  /**
   * 
   * FIM - Paginação
   * 
   */
  render() {
    return (
      <Layout className="layout">
        <StyledHeader>
          <div className="logo" />
          <StyledMenu
            mode="horizontal"
            defaultSelectedKeys={['2']}
            style={{ lineHeight: '64px' }}
          >
            <StyledMenuItem key="1">Início</StyledMenuItem>
            <StyledMenuItem key="2">Pré-Matrículas</StyledMenuItem>
            <Button>
              <NavLink to="/logout">
                Sair
              </NavLink>
            </Button>
          </StyledMenu>

        </StyledHeader>
        <Content style={{ padding: '0 50px' }}>
          <div style={{ padding: 24, background: 'white', textAlign: 'center', maxHeight: 900, minHeight: 900 }} >

            <StyledCard>
              <h2>LISTA DE PRÉ-MATRÍCULAS</h2>
              {this.state.isLoadingTable ?
                // <p>Carregando...</p>
                <Skeleton active paragraph={{ rows: 10 }} title={{ width: 20 }} />
                :
                <>
                  {this.state.msgError !== "" ?
                    <p>{this.state.msgError}</p>
                    :
                    <>
                      {this.state.preMatriculaList.length === 0 ?
                        <p>Não possui matriculas cadastradas até o momento.</p>
                        :
                        <>
                          <Table
                            thead={['Nome', 'Idade', 'Turma', 'Horário', 'Data', 'Responsável', 'Telefone', 'E-mail']}
                            tbody={this.state.preMatriculaList}
                          />
                        </>
                      }
                    </>
                  }
                </>
              }

            </StyledCard>
            <Pagination
              currentPage={this.state.currentPage}
              totalResults={this.state.totalResults}
              limit={this.state.limit}
              goToPage={this.goToPage}
            />
          </div>

        </Content>
        <Footer style={{ textAlign: 'center' }}>Happy Code Aracaju ©2019 Created by GetInfo</Footer>
      </Layout>
    );
  }
}

const StyledCard = styled.div`
  width: auto;
  max-width: 1000px;
  margin: 0 auto;  
  box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
  background: white;
  padding: 30px;
  min-Height: 700px;
  margin-bottom:10px;
  margin-top:30px;

  h2 {
    text-align:center;
  }

  table {
    
    height: auto;
    border: 1px solid black;
    overflow-x: scroll;
    width: 100%;
    max-width: 1500px;
    border-collapse: collapse;
    border: 2px;

    thead {
      text-align:center;
      background: rgb(137, 92, 242);
    }
    
    thead, tbody {
      font-family: sans-serif;
      color: rgba(0,0,0,0.87);
      line-height: 1.7;
      font-size: 15px;
      -webkit-font-smoothing: antialiased;
      text-align: center;
    }

    thead th{
      color: #fff;
      text-transform:uppercase;
    }

    tbody tr:hover{
      background-color: rgb(183, 183, 183,0.5);
    }

    tbody td{
      font-size: 13px;
    }
  }
`;

const StyledHeader = styled(Header)` 

    background: #7FD38F !important;

    .logo{
      width: 50px;
      height: 50px;
      background-image: url(${logo1});
      background-repeat: no-repeat;
      margin: 8px 16px 8px 16px;
      float: left;
    }
`;

const StyledMenu = styled(Menu)`

    background: #7FD38F !important;

    .ant-menu-item-selected{
      color: #895CF2 !important;
      border-bottom:2px solid #895CF2 !important;
    }

    .ant-menu-item:hover{
      color: #895CF2 !important;
      border-bottom:2px solid #895CF2 !important;
    }

    button{
      position:relative;
      float: right;
      background:#fff;
      color: #895CF2;
      border: 1px solid #895CF2 ;
      margin: 13px;
    }
    button:hover{
      background: #895CF2;
      color: #fff;
      border: 1px solid #895CF2 ;
      margin: 13px;
    }
      

`;

const StyledMenuItem = styled(Menu.Item)`

    color: #fff;
    font-weight: bold;
  
`;


export default PreMatriculas;
