import React from "react";
import { Redirect } from "react-router-dom";
import auth from "../auth";

const Logout = () => {
  if (auth.logout()) {
    return <Redirect to="/login" />;
  }
};

export default Logout;
