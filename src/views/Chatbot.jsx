import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import ChatBot from 'react-simple-chatbot';
import Axios from 'axios';
import { ThemeProvider } from 'styled-components';
import scripts from '../scripts';
import styled from 'styled-components';
//imagens
import img1 from '../assets/images/happycode.png';
import img2 from '../assets/images/whatsapp.png';
//componentes
import Gifs from '../components/Gifs/index.jsx';
import Links from '../components/Links/index.jsx';
//gifs
import gif1 from '../assets/gifs/neymaragradecendo.gif';
import gif2 from '../assets/gifs/mindblow.gif';
import gif3 from '../assets/gifs/jaentendi.gif';


//https://api.whatsapp.com/send?phone=5579991800901&text=Olá!%20Tenho%20interesse%20em%20saber%20mais%20sobre%20Os%20Cursos%20da%20Happy%20Code%20Aracaju

export default class Chatbot extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      freetime: '',
      age: '',
      classroom: '',
      schedule: '',
      parentsName: '',
      parentsEmail: '',
      parentsContact: '',

      steps: [
        {
          id: '1',
          message: 'Olá, tudo bem? Você quer ajuda para escolher a melhor opção dentre as muitas coisas legais que temos? Vamos começar?',
          trigger: 'intro',
        },
        {
          id: 'intro',
          options: [
            { value: 'sim', label: 'Sim', trigger: '3' },
            { value: 'sim2', label: 'Com certeza', trigger: '3' },
          ],
        },
        {
          id: '3',
          asMessage: true,
          component: (
            <Gifs img={gif1} />
          ),
          trigger: '4',
        },
        {
          id: '4',
          message: 'Legal! Vou te ajudar a achar uma excelente opção... Vamos lá!',
          trigger: '5',
        },
        {
          id: '5',
          message: 'Mas antes, preciso saber mais sobre o nosso futuro aluno(a). Qual o nome dele(a)?',
          trigger: 'name',
        },
        {
          id: 'name',
          user: true,
          validator: (value) => {
            if (value === '') {
              return 'Digite um nome';
            }
            if (!scripts.validadeName(value)) {
              return 'Nome Invalido'
            }
            return true;
          },
          trigger: (e) => {
            console.log(e);
            this.setState({ name: e.value });
            return '7'
          },
        },
        {
          id: '7',
          message: 'na sua opinião, qual a atividade que {previousValue} mais gosta de fazer nas horas vagas em casa?',
          trigger: 'freetime',
        },
        //MENSAGEM PARA QUANDO O USUÁRIO VOLTAR A CONVERSA
        {
          id: '19',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Ok, então vamos recomeçar... qual a atividade que ${nameCrop} mais gosta de fazer nas horas vagas em casa?`
          },
          trigger: 'freetime',
        },
        {
          id: 'freetime',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'Ver e gravar vídeos', label: 'Ver e gravar vídeos',
                trigger: () => {
                  this.setState({ freetime: 'Ver e gravar vídeos' });
                  return '1000'
                },
              },
              {
                value: 'Jogar Video Games', label: 'Jogar Video Games',
                trigger: () => {
                  this.setState({ freetime: 'Jogar Video Games' });
                  return '2000'
                },
              },
              {
                value: 'Criar e montar coisas', label: 'Criar e montar coisas',
                trigger: () => {
                  this.setState({ freetime: 'Criar e montar coisas' });
                  return '3000'
                },
              },
            ],
        },
        // MENSAGEM DO INTERESSE JOGAR VIDEO GAMES
        {
          id: '2000',
          message: 'Que Legal! Você sabia que já existem pesquisas que mostram os benefícios de jogar vídeo game? Pesquisadores constataram que jogos eletrônicos desenvolvem regiões do cérebro responsáveis pela navegação espacial, formação de memória, habilidades motoras e até mesmo planejamento estratégico!',
          
          trigger: '2001',
        },
        //MENSAGEM IDADE 1 JOGAR VIDEO GAMES
        {
          id: '2001',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Qual a faixa etária de ${nameCrop}?`
          },
          trigger: 'age1',
        },
        {
          id: 'age1',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: '5 - 6 anos', label: '5 - 6 anos',
                trigger: () => {
                  this.setState({ age: '5 - 6 anos' });
                  return '2020'
                },
              },
              {
                value: '7 - 10 anos', label: '7 - 10 anos',
                trigger: () => {
                  this.setState({ age: '7 - 10 anos' });
                  return '2020'
                },
              },
              {
                value: '11 - 13 anos', label: '11 - 13 anos',
                trigger: () => {
                  this.setState({ age: '11 - 13 anos' });
                  return '2030'
                },
              },
            ],
        },
        //CURSOS DA IDADE 5 - 6 ANOS JOGAR VIDEO GAME     ||||||||||
        {
          id: '2020',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Hmmm... com base na idade e nas atividades de ${nameCrop}, ja sei o que pode ser legal!!!!?`
          },
          trigger: '2021',
        },
        {
          id: '2021',
          asMessage: true,
          component: (
            <Gifs img={gif3} />
          ),
          trigger: '2012',
        },
        {
          id: '2012',
          message: 'Clique em uma das opções abaixo!!',
          trigger: 'classroom1',
        },
        {
          id: 'classroom1',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'Scracth Jr', label: 'Scracth Jr',
                trigger: () => {
                  this.setState({ classroom: 'Scracth Jr' });
                  return '2100'
                },
              },
              {
                value: 'Hmmm... quero voltar', label: 'Hmmm... quero voltar',
                trigger: () => {
                  this.setState({ classroom: 'Hmmm... quero voltar' });
                  return '19'
                },
              },
            ],
        },
        //MENSAGEM DA EXPLICAÇÃO DO CURSO
        {
          id: '2100',
          message: 'O objetivo deste curso é ensinar de forma prática e divertida conceitos computacionais relacionados à programação e ao desenvolvimento de jogos, de forma interdisciplinar com as áreas de Ciências, Tecnologia, Engenharia e Matemática (STEM). Os alunos serão desafiados a desenvolver jogos usando a ferramenta Scratch Jr',
          trigger: '2101',
        },
        {
          id: '2101',
          options: [
            { value: '1', label: 'Legal!! Quais são os dias e valores', trigger: 'valor1' },
            { value: '2', label: 'É este curso mesmo, quero matricular!', trigger: 'valor1' },
            { value: '3', label: 'Não sei, quero ver outra opção!', trigger: '19' },
          ],
        },
        {
          id: 'valor1',
          message: 'O investimento é de: R$ 3.600 ou 1 + 11 parcelas de: R$ 300,00. Material didádito anual: R$ 360,00 (até 12x no cartão de crédito).',
          trigger: 'schedule1',
        },
        //HORARIO DO CURSO SCRACTH DA IDADE 5-6 DO INTERESSE JOGAR VIDEO GAME
        {
          id: 'schedule1',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'QUARTA | 10:30 - 12:00', label: 'QUARTA | 10:30 - 12:00',
                trigger: () => {
                  this.setState({ schedule: 'QUARTA | 10:30 - 12:00' });
                  return '20'
                },
              },
              {
                value: 'SEXTA | 13:30 - 15:00', label: 'SEXTA | 13:30 - 15:00',
                trigger: () => {
                  this.setState({ schedule: 'SEXTA | 13:30 - 15:00' });
                  return '20'
                },
              },
              {
                value: 'Hmmm... quero ver outras opções', label: 'Hmmm... quero ver outras opções',
                trigger: () => {
                  this.setState({ schedule: 'Hmmm... quero ver outras opções' });
                  return '19'
                },
              },
            ],
        },
        //CURSOS DA IDADE 7 - 10 ANOS JOGAR VIDEO GAME      ||||||||||
        {
          id: '2020',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Hmmm... com base na idade e nas atividades de ${nameCrop}, ja sei o que pode ser legal!!!!?`
          },
          trigger: '2021',
        },
        {
          id: '2021',
          asMessage: true,
          component: (
            <Gifs img={gif3} />
          ),
          trigger: '2022',
        },
        {
          id: '2022',
          message: 'Clique em uma das opções abaixo!!',
          trigger: 'classroom1',
        },
        {
          id: 'classroom1',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'LET Games 2D - Iniciante', label: 'LET Games 2D - Iniciante',
                trigger: () => {
                  this.setState({ classroom: 'LET Games 2D - Iniciante' });
                  return '2200'
                },
              },
              {
                value: 'Hmmm... quero voltar', label: 'Hmmm... quero voltar',
                trigger: () => {
                  this.setState({ classroom: 'Hmmm... quero voltar' });
                  return '19'
                },
              },
            ],
        },
        //MENSAGEM DA EXPLICAÇÃO DO CURSO
        {
          id: '2200',
          message: 'Nesse  curso,  os  alunos  aprenderão  os  conceitos de  criação  de  jogos  2D,  serão capazes  de  desenvolver  jogos  2D completo, com condições lógicas baseada  em eventos,  backgrounds, personagens,  interface  gráfica  e  todos  os  conceitos inerentes  da  produção  de  jogos,  serão  induzidos  a  criar  o  seu  próprio  partindo totalmente de um papel em branco.',
          trigger: '2201',
        },
        {
          id: '2201',
          options: [
            { value: '1', label: 'Legal!! Quais são os dias e valores', trigger: 'valor12' },
            { value: '2', label: 'É este curso mesmo, quero matricular!', trigger: 'valor12' },
            { value: '3', label: 'Não sei, quero ver outra opção!', trigger: '19' },
          ],
        },
        {
          id: 'valor12',
          message: 'O investimento é de: R$ 3.600 ou 1 + 11 parcelas de: R$ 300,00. Material didádito anual: R$ 360,00 (até 12x no cartão de crédito).',
          trigger: 'schedule12',
        },
        //HORARIO DO CURSO LET Games 2D - Iniciante DA IDADE 7 - 10 DO INTERESSE JOGAR VIDEO GAME
        {
          id: 'schedule12',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'SEGUNDA | 9:00 - 10:30', label: 'SEGUNDA | 9:00 - 10:30',
                trigger: () => {
                  this.setState({ schedule: 'SEGUNDA | 9:00 - 10:30' });
                  return '20'
                },
              },
              {
                value: 'SEXTA | 10:30 - 12:00', label: 'SEXTA | 13:30 - 15:00',
                trigger: () => {
                  this.setState({ schedule: 'SEXTA | 13:30 - 15:00' });
                  return '20'
                },
              },
              {
                value: 'Hmmm... quero ver outras opções', label: 'Hmmm... quero ver outras opções',
                trigger: () => {
                  this.setState({ schedule: 'Hmmm... quero ver outras opções' });
                  return '19'
                },
              },
            ],
        },
        //CURSOS DA IDADE 11 - 14 ANOS JOGAR VIDEO GAME     ||||||||||
        {
          id: '2030',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Hmmm... com base na idade e nas atividades de ${nameCrop}, ja sei o que pode ser legal!!!!?`
          },
          trigger: '2031',
        },
        {
          id: '2031',
          asMessage: true,
          component: (
            <Gifs img={gif3} />
          ),
          trigger: '2032',
        },
        {
          id: '2032',
          message: 'Clique em uma das opções abaixo!!',
          trigger: 'classroom1',
        },
        {
          id: 'classroom1',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'LET Games 3D - Iniciante', label: 'LET Games 3D - Iniciante',
                trigger: () => {
                  this.setState({ classroom: 'LET Games 3D - Iniciante' });
                  return '2300'
                },
              },
              {
                value: 'Hmmm... quero voltar', label: 'Hmmm... quero voltar',
                trigger: () => {
                  this.setState({ classroom: 'Hmmm... quero voltar' });
                  return '19'
                },
              },
            ],
        },
        //MENSAGEM DA EXPLICAÇÃO DO CURSO
        {
          id: '2300',
          message: 'Ao final deste curso o aluno conseguirá compreender o funcionamento e a criação de aplicativos para tablets e smartphones com o sistema operacional Android. Outro fator importante neste curso que o aluno entenderá será como programar e o que é linguagem de programação, já que utilizaram a linguagem Java, e com tudo isso conseguiram ao final criar o(s) próprio(s) aplicativo(s)',
          trigger: '2301',
        },
        {
          id: '2301',
          options: [
            { value: '1', label: 'Legal!! Quais são os dias e valores', trigger: 'valor13' },
            { value: '2', label: 'É este curso mesmo, quero matricular!', trigger: 'valor13' },
            { value: '3', label: 'Não sei, quero ver outra opção!', trigger: '19' },
          ],
        },
        {
          id: 'valor13',
          message: 'O investimento é de: R$ 3.600 ou 1 + 11 parcelas de: R$ 300,00. Material didádito anual: R$ 360,00 (até 12x no cartão de crédito).',
          trigger: 'schedule13',
        },
        //HORARIO DO CURSO LET Games 3D - Iniciante DA IDADE 11 - 14 DO INTERESSE JOGAR VIDEO GAME
        {
          id: 'schedule13',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'SEGUNDA | 13:30 - 15:00', label: 'SEGUNDA | 13:30 - 15:00',
                trigger: () => {
                  this.setState({ schedule: 'SEGUNDA | 13:30 - 15:00' });
                  return '20'
                },
              },
              {
                value: 'QUARTA | 9:00 - 10:30', label: 'QUARTA | 9:00 - 10:30',
                trigger: () => {
                  this.setState({ schedule: 'QUARTA | 9:00 - 10:30' });
                  return '20'
                },
              },
              {
                value: 'SABADO | 10:30 - 12:00', label: 'SABADO | 10:30 - 12:00',
                trigger: () => {
                  this.setState({ schedule: 'SABADO | 10:30 - 12:00' });
                  return '20'
                },
              },
              {
                value: 'Hmmm... quero ver outras opções', label: 'Hmmm... quero ver outras opções',
                trigger: () => {
                  this.setState({ schedule: 'Hmmm... quero ver outras opções' });
                  return '19'
                },
              },
            ],
        },

        /////////////////////////
        /////////////////////////
        /////////////////////////
        /////////////////////////
        /////////////////////////
        /////////////////////////

        //MENSAGEM DO INTERESSE VER E GRAVAR VIDEOS
        {
          id: '1000',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Bem, pelo que você nos informou, ${nameCrop} deve adorar ver videos no Youtube, e provavelmente quer ter (ou já tem) seu próprio canal. Compreendemos perfeitamente a preocupação de todos os pais com a exposição de seus filhos.`
          },
          trigger: '1001',
        },
        //MENSAGEM IDADE 1 VER E GRAVAR VIDEOS
        {
          id: '1001',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Qual a faixa etária de ${nameCrop}?`
          },
          trigger: 'age2',
        },
        {
          id: 'age2',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: '5 - 6 anos', label: '5 - 6 anos',
                trigger: () => {
                  this.setState({ age: '5 - 6 anos' });
                  return '1010'
                },
              },
              {
                value: '7 - 10 anos', label: '7 - 10 anos',
                trigger: () => {
                  this.setState({ age: '7 - 10 anos' });
                  return '1020'
                },
              },
              {
                value: '11 - 13 anos', label: '11 - 13 anos',
                trigger: () => {
                  this.setState({ age: '11 - 13 anos' });
                  return '1030'
                },
              },
              {
                value: '14 - 17 anos', label: '14 - 17 anos',
                trigger: () => {
                  this.setState({ age: '14 - 17 anos' });
                  return '1040'
                },
              },
            ],
        },
        //CURSOS DA IDADE 5 - 6 ANOS VER E GRAVAR VIDEOS     ||||||||||
        {
          id: '1010',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Hmmm... com base na idade e nas atividades de ${nameCrop}, ja sei o que pode ser legal!!!!?`
          },
          trigger: '1011',
        },
        {
          id: '1011',
          asMessage: true,
          component: (
            <Gifs img={gif3} />
          ),
          trigger: '1012',
        },
        {
          id: '1012',
          message: 'Clique em uma das opções abaixo!!',
          trigger: 'classroom2',
        },
        {
          id: 'classroom2',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'Youtuber Jr', label: 'Youtuber Jr',
                trigger: () => {
                  this.setState({ classroom: 'Youtuber Jr' });
                  return '1100'
                },
              },
              {
                value: 'Hmmm... quero voltar', label: 'Hmmm... quero voltar',
                trigger: () => {
                  this.setState({ classroom: 'Hmmm... quero voltar' });
                  return '19'
                },
              },
            ],
        },
        //MENSAGEM DA EXPLICAÇÃO DO CURSO
        {
          id: '1100',
          message: 'Os alunos serão capazes de criar um canal no Youtube, customizar o canal criando uma identidade  visual,  gerar  conteúdos  de  qualidade  usando  técnicas  de  roteirização, gravação  e  edição  de  áudio  e  vídeo.  Disponibilizar  o conteúdo  criado  através  do  seu  canal,  de  acordo  com  seu  público  alvo e  sem  ferir  os direitos autorais.',
          trigger: '1101',
        },
        {
          id: '1101',
          options: [
            { value: '1', label: 'Legal!! Quais são os dias e valores', trigger: 'valor2' },
            { value: '2', label: 'É este curso mesmo, quero matricular!', trigger: 'valor2' },
            { value: '3', label: 'Não sei, quero ver outra opção!', trigger: '19' },
          ],
        },
        {
          id: 'valor2',
          message: 'O investimento é de: R$3.600 ou 1 + 11 parcelas de: R$ 300,00. Material didádito anual: R$ 360,00 (até 12x no cartão de crédito).',
          trigger: 'schedule2',
        },
        //HORARIO DO CURSO YOUTUBER JR DA IDADE 5-6 DO INTERESSE VER E GRAVAR VÍDEOS
        {
          id: 'schedule2',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'QUARTA | 13:30 - 15:00', label: 'QUARTA | 13:30 - 15:00',
                trigger: () => {
                  this.setState({ schedule: 'QUARTA | 13:30 - 15:00' });
                  return '20'
                },
              },
              {
                value: 'SEXTA | 9:00 - 10:30', label: 'SEXTA | 9:00 - 10:30',
                trigger: () => {
                  this.setState({ schedule: 'SEXTA | 9:00 - 10:30' });
                  return '20'
                },
              },
              {
                value: 'Hmmm... quero ver outras opções', label: 'Hmmm... quero ver outras opções',
                trigger: () => {
                  this.setState({ schedule: 'Hmmm... quero ver outras opções' });
                  return '19'
                },
              },
            ],
        },
        //CURSOS DA IDADE 7 - 10 ANOS VER E GRAVAR VIDEOS   ||||||||||
        {
          id: '1020',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Hmmm... com base na idade e nas atividades de ${nameCrop}, ja sei o que pode ser legal!!!!?`
          },
          trigger: '1021',
        },
        {
          id: '1021',
          asMessage: true,
          component: (
            <Gifs img={gif3} />
          ),
          trigger: '1022',
        },
        {
          id: '1022',
          message: 'Clique em uma das opções abaixo!!',
          trigger: 'classroom2',
        },
        {
          id: 'classroom2',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'Youtuber Jr', label: 'Youtuber Jr',
                trigger: () => {
                  this.setState({ classroom: 'Youtuber Jr' });
                  return '1200'
                },
              },
              {
                value: 'Hmmm... quero voltar', label: 'Hmmm... quero voltar',
                trigger: () => {
                  this.setState({ classroom: 'Hmmm... quero voltar' });
                  return '19'
                },
              },
            ],
        },
        //MENSAGEM DA EXPLICAÇÃO DO CURSO
        {
          id: '1200',
          message: 'Os alunos serão capazes de criar um canal no Youtube, customizar o canal criando uma identidade  visual,  gerar  conteúdos  de  qualidade  usando  técnicas  de  roteirização, gravação  e  edição  de  áudio  e  vídeo.  Disponibilizar  o conteúdo  criado  através  do  seu  canal,  de  acordo  com  seu  público  alvo e  sem  ferir  os direitos autorais.',
          trigger: '1201',
        },
        {
          id: '1201',
          options: [
            { value: '1', label: 'Legal!! Quais são os dias e valores', trigger: 'valor21' },
            { value: '2', label: 'É este curso mesmo, quero matricular!', trigger: 'valor21' },
            { value: '3', label: 'Não sei, quero ver outra opção!', trigger: '19' },
          ],
        },
        {
          id: 'valor21',
          message: 'O investimento é de: R$3.600 ou 1 + 11 parcelas de: R$ 300,00. Material didádito anual: R$ 360,00 (até 12x no cartão de crédito).',
          trigger: 'schedule21',
        },
        //HORARIO DO CURSO YOUTUBER JR DA IDADE 7 - 10 DO INTERESSE VER E GRAVAR VÍDEOS
        {
          id: 'schedule21',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'QUARTA | 13:30 - 15:00', label: 'QUARTA | 13:30 - 15:00',
                trigger: () => {
                  this.setState({ schedule: 'QUARTA | 13:30 - 15:00' });
                  return '20'
                },
              },
              {
                value: 'SEXTA | 9:00 - 10:30', label: 'SEXTA | 9:00 - 10:30',
                trigger: () => {
                  this.setState({ schedule: 'SEXTA | 9:00 - 10:30' });
                  return '20'
                },
              },
              {
                value: 'Hmmm... quero ver outras opções', label: 'Hmmm... quero ver outras opções',
                trigger: () => {
                  this.setState({ schedule: 'Hmmm... quero ver outras opções' });
                  return '19'
                },
              },
            ],
        },
        //CURSOS DA IDADE 7 - 10 ANOS VER E GRAVAR VIDEOS   ||||||||||
        {
          id: '1020',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Hmmm... com base na idade e nas atividades de ${nameCrop}, ja sei o que pode ser legal!!!!?`
          },
          trigger: '1021',
        },
        {
          id: '1021',
          asMessage: true,
          component: (
            <Gifs img={gif3} />
          ),
          trigger: '1022',
        },
        {
          id: '1022',
          message: 'Clique em uma das opções abaixo!!',
          trigger: 'classroom2',
        },
        {
          id: 'classroom2',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'Youtuber Jr', label: 'Youtuber Jr',
                trigger: () => {
                  this.setState({ classroom: 'Youtuber Jr' });
                  return '1200'
                },
              },
              {
                value: 'Hmmm... quero voltar', label: 'Hmmm... quero voltar',
                trigger: () => {
                  this.setState({ classroom: 'Hmmm... quero voltar' });
                  return '19'
                },
              },
            ],
        },
        //MENSAGEM DA EXPLICAÇÃO DO CURSO
        {
          id: '1200',
          message: 'Os alunos serão capazes de criar um canal no Youtube, customizar o canal criando uma identidade  visual,  gerar  conteúdos  de  qualidade  usando  técnicas  de  roteirização, gravação  e  edição  de  áudio  e  vídeo.  Disponibilizar  o conteúdo  criado  através  do  seu  canal,  de  acordo  com  seu  público  alvo e  sem  ferir  os direitos autorais.',
          trigger: '1201',
        },
        {
          id: '1201',
          options: [
            { value: '1', label: 'Legal!! Quais são os dias e valores', trigger: 'valor22' },
            { value: '2', label: 'É este curso mesmo, quero matricular!', trigger: 'valor22' },
            { value: '3', label: 'Não sei, quero ver outra opção!', trigger: '19' },
          ],
        },
        {
          id: 'valor22',
          message: 'O investimento é de: R$3.600 ou 1 + 11 parcelas de: R$ 300,00. Material didádito anual: R$ 360,00 (até 12x no cartão de crédito).',
          trigger: 'schedule22',
        },
        //HORARIO DO CURSO YOUTUBER JR DA IDADE 7 - 10 DO INTERESSE VER E GRAVAR VÍDEOS
        {
          id: 'schedule22',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'QUARTA | 13:30 - 15:00', label: 'QUARTA | 13:30 - 15:00',
                trigger: () => {
                  this.setState({ schedule: 'QUARTA | 13:30 - 15:00' });
                  return '20'
                },
              },
              {
                value: 'SEXTA | 9:00 - 10:30', label: 'SEXTA | 9:00 - 10:30',
                trigger: () => {
                  this.setState({ schedule: 'SEXTA | 9:00 - 10:30' });
                  return '20'
                },
              },
              {
                value: 'Hmmm... quero ver outras opções', label: 'Hmmm... quero ver outras opções',
                trigger: () => {
                  this.setState({ schedule: 'Hmmm... quero ver outras opções' });
                  return '19'
                },
              },
            ],
        },
        //CURSOS DA IDADE 11 - 13 ANOS VER E GRAVAR VIDEOS   ||||||||||
        {
          id: '1030',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Hmmm... com base na idade e nas atividades de ${nameCrop}, ja sei o que pode ser legal!!!!?`
          },
          trigger: '1031',
        },
        {
          id: '1031',
          asMessage: true,
          component: (
            <Gifs img={gif3} />
          ),
          trigger: '1032',
        },
        {
          id: '1032',
          message: 'Clique em uma das opções abaixo!!',
          trigger: 'classroom2',
        },
        {
          id: 'classroom2',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'Youtuber Jr', label: 'Youtuber Jr',
                trigger: () => {
                  this.setState({ classroom: 'Youtuber Jr' });
                  return '1300'
                },
              },
              {
                value: 'Hmmm... quero voltar', label: 'Hmmm... quero voltar',
                trigger: () => {
                  this.setState({ classroom: 'Hmmm... quero voltar' });
                  return '19'
                },
              },
            ],
        },
        //MENSAGEM DA EXPLICAÇÃO DO CURSO
        {
          id: '1300',
          message: 'TOs alunos serão capazes de criar um canal no Youtube, customizar o canal criando uma identidade  visual,  gerar  conteúdos  de  qualidade  usando  técnicas  de  roteirização, gravação  e  edição  de  áudio  e  vídeo.  Disponibilizar  o conteúdo  criado  através  do  seu  canal,  de  acordo  com  seu  público  alvo e  sem  ferir  os direitos autorais.',
          trigger: '1301',
        },
        {
          id: '1301',
          options: [
            { value: '1', label: 'Legal!! Quais são os dias e valores', trigger: 'valor3' },
            { value: '2', label: 'É este curso mesmo, quero matricular!', trigger: 'valor3' },
            { value: '3', label: 'Não sei, quero ver outra opção!', trigger: '19' },
          ],
        },
        {
          id: 'valor3',
          message: 'O investimento é de: R$3.600 ou 1 + 11 parcelas de: R$ 300,00. Material didádito anual: R$ 360,00 (até 12x no cartão de crédito).',
          trigger: 'schedule3',
        },
        //HORARIO DO CURSO YOUTUBER JR DA IDADE 11 - 13 DO INTERESSE VER E GRAVAR VÍDEOS
        {
          id: 'schedule3',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'QUARTA | 13:30 - 15:00', label: 'QUARTA | 13:30 - 15:00',
                trigger: () => {
                  this.setState({ schedule: 'QUARTA | 13:30 - 15:00' });
                  return '20'
                },
              },
              {
                value: 'SEXTA | 9:00 - 10:30', label: 'SEXTA | 9:00 - 10:30',
                trigger: () => {
                  this.setState({ schedule: 'SEXTA | 9:00 - 10:30' });
                  return '20'
                },
              },
              {
                value: 'Hmmm... quero ver outras opções', label: 'Hmmm... quero ver outras opções',
                trigger: () => {
                  this.setState({ schedule: 'Hmmm... quero ver outras opções' });
                  return '19'
                },
              },
            ],
        },
        //CURSOS DA IDADE 14 -17 ANOS VER E GRAVAR VIDEOS   ||||||||||
        {
          id: '1040',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Hmmm... com base na idade e nas atividades de ${nameCrop}, ja sei o que pode ser legal!!!!?`
          },
          trigger: '1041',
        },
        {
          id: '1041',
          asMessage: true,
          component: (
            <Gifs img={gif3} />
          ),
          trigger: '1042',
        },
        {
          id: '1042',
          message: 'Clique em uma das opções abaixo!!',
          trigger: 'classroom2',
        },
        {
          id: 'classroom2',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'Youtuber Jr', label: 'Youtuber Jr',
                trigger: () => {
                  this.setState({ classroom: 'Youtuber Jr' });
                  return '1200'
                },
              },
              {
                value: 'Hmmm... quero voltar', label: 'Hmmm... quero voltar',
                trigger: () => {
                  this.setState({ classroom: 'Hmmm... quero voltar' });
                  return '19'
                },
              },
            ],
        },
        //MENSAGEM DA EXPLICAÇÃO DO CURSO                    
        {
          id: '1200',
          message: 'Os alunos serão capazes de criar um canal no Youtube, customizar o canal criando uma identidade  visual,  gerar  conteúdos  de  qualidade  usando  técnicas  de  roteirização, gravação  e  edição  de  áudio  e  vídeo.  Disponibilizar  o conteúdo  criado  através  do  seu  canal,  de  acordo  com  seu  público  alvo e  sem  ferir  os direitos autorais.',
          trigger: '1201',
        },
        {
          id: '1201',
          options: [
            { value: '1', label: 'Legal!! Quais são os dias e valores', trigger: 'valor31' },
            { value: '2', label: 'É este curso mesmo, quero matricular!', trigger: 'valor31' },
            { value: '3', label: 'Não sei, quero ver outra opção!', trigger: '19' },
          ],
        },
        {
          id: 'valor31',
          message: 'O investimento é de: R$3.600 ou 1 + 11 parcelas de: R$ 300,00. Material didádito anual: R$ 360,00 (até 12x no cartão de crédito).',
          trigger: 'schedule31',
        },
        //HORARIO DO CURSO YOUTUBER JR DA IDADE 14 - 17 DO INTERESSE VER E GRAVAR VÍDEOS
        {
          id: 'schedule31',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'QUARTA | 13:30 - 15:00', label: 'QUARTA | 13:30 - 15:00',
                trigger: () => {
                  this.setState({ schedule: 'QUARTA | 13:30 - 15:00' });
                  return '20'
                },
              },
              {
                value: 'SEXTA | 9:00 - 10:30', label: 'SEXTA | 9:00 - 10:30',
                trigger: () => {
                  this.setState({ schedule: 'SEXTA | 9:00 - 10:30' });
                  return '20'
                },
              },
              {
                value: 'Hmmm... quero ver outras opções', label: 'Hmmm... quero ver outras opções',
                trigger: () => {
                  this.setState({ schedule: 'Hmmm... quero ver outras opções' });
                  return '19'
                },
              },
            ],
        },
        /////////////////////////
        /////////////////////////
        /////////////////////////
        /////////////////////////
        /////////////////////////
        /////////////////////////

        //MENSAGEM DO INTERESSE CRIAR E MONTAR COISAS
        {
          id: '3000',
          message: 'Que legal! É muito importante despertar o interesse dos nossos pequenos sobre como as coisas funcionam e como elas são criadas.',
          trigger: '3001',
        },
        //MENSAGEM IDADE 3 CRIAR E MONTAR COISAS
        {
          id: '3001',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Qual a faixa etária de ${nameCrop}?`
          },
          trigger: 'age3',
        },
        {
          id: 'age3',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: '7 - 10 anos', label: '7 - 10 anos',
                trigger: () => {
                  this.setState({ age: '7 - 10 anos' });
                  return '3020'
                },
              },
              {
                value: '11 - 13 anos', label: '11 - 13 anos',
                trigger: () => {
                  this.setState({ age: '11 - 13 anos' });
                  return '3030'
                },
              },
              {
                value: '14 - 17 anos', label: '14 - 17 anos',
                trigger: () => {
                  this.setState({ age: '14 - 17 anos' });
                  return '3040'
                },
              },
            ],
        },
        //CURSOS DA IDADE 7 - 10 ANOS CRIAR E MONTAR COISAS   ||||||||||
        {
          id: '3020',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Hmmm... com base na idade e nas atividades de ${nameCrop}, ja sei o que pode ser legal!!!!?`
          },
          trigger: '3021',
        },
        {
          id: '3021',
          asMessage: true,
          component: (
            <Gifs img={gif3} />
          ),
          trigger: '3022',
        },
        {
          id: '3022',
          message: 'Clique em uma das opções abaixo!!',
          trigger: 'classroom3',
        },
        {
          id: 'classroom3',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'LET Aplicativos Iniciantes', label: 'LET Aplicativos Iniciantes',
                trigger: () => {
                  this.setState({ classroom: 'LET Aplicativos Iniciantes' });
                  return '3100'
                },
              },
              {
                value: 'LET IOT Maker', label: 'LET IOT Maker',
                trigger: () => {
                  this.setState({ classroom: 'LET IOT Maker' });
                  return '3200'
                },
              },
              {
                value: 'LET Drone 1', label: 'LET Drone 1',
                trigger: () => {
                  this.setState({ classroom: 'LET Drone 1' });
                  return '3300'
                },
              },
              {
                value: 'Hmmm... quero voltar', label: 'Hmmm... quero voltar',
                trigger: () => {
                  this.setState({ classroom: 'Hmmm... quero voltar' });
                  return '19'
                },
              },
            ],
        },
        //MENSAGEM DA EXPLICAÇÃO DO CURSO LET APLICATIVOS INICIANTES
        {
          id: '3100',
          message: 'Ao final deste curso o aluno conseguirá compreender o funcionamento e a criação de aplicativos para tablets e smartphones com o sistema operacional Android. Outro fator importante neste curso que o aluno entenderá será como programar e o que é linguagem de programação, já que utilizaram a linguagem Java, e com tudo isso conseguiram ao final criar o(s) próprio(s) aplicativo(s).',
          trigger: '3101',
        },
        {
          id: '3101',
          options: [
            { value: '1', label: 'Legal!! Quais são os dias e valores', trigger: 'valor32' },
            { value: '2', label: 'É este curso mesmo, quero matricular!', trigger: 'valor32' },
            { value: '3', label: 'Não sei, quero ver outra opção!', trigger: '19' },
          ],
        },
        {
          id: 'valor32',
          message: 'O investimento é de: R$3.600 ou 1 + 11 parcelas de: R$ 300,00. Material didádito anual: R$ 360,00 (até 12x no cartão de crédito).',
          trigger: 'schedule32',
        },
        //HORARIO DO CURSO LET APLICATIVOS INICIANTES DA IDADE 7 - 10 DO INTERESSE CRIAR E MONTAR COISAS
        {
          id: 'schedule32',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'TERÇA | 16:30 - 18:00', label: 'TERÇA | 16:30 - 18:00',
                trigger: () => {
                  this.setState({ schedule: 'TERÇA | 16:30 - 18:00' });
                  return '20'
                },
              },
              {
                value: 'QUINTA | 9:00 - 10:30', label: 'QUINTA | 9:00 - 10:30',
                trigger: () => {
                  this.setState({ schedule: 'QUINTA | 9:00 - 10:30' });
                  return '20'
                },
              },
              {
                value: 'SEXTA | 15:00 - 16:30', label: 'SEXTA | 15:00 - 16:30',
                trigger: () => {
                  this.setState({ schedule: 'SEXTA | 15:00 - 16:30' });
                  return '20'
                },
              },
              {
                value: 'Hmmm... quero ver outras opções', label: 'Hmmm... quero ver outras opções',
                trigger: () => {
                  this.setState({ schedule: 'Hmmm... quero ver outras opções' });
                  return '19'
                },
              },
            ],
        },

        //MENSAGEM DA EXPLICAÇÃO DO CURSO LET IOT MAKER
        {
          id: '3200',
          message: 'Ao concluir este curso os alunos serão capazes de construir aplicações e dispositivos inteligentes que utilizem robótica e internet das coisas, com base no movimento DYI – Do it yourself (faça você mesmo). Os estudantes precisarão usar a criatividade para criar protótipos e soluções customizadas.',
          trigger: '3201',
        },
        {
          id: '3201',
          options: [
            { value: '1', label: 'Legal!! Quais são os dias e valores', trigger: 'valor33' },
            { value: '2', label: 'É este curso mesmo, quero matricular!', trigger: 'valor33' },
            { value: '3', label: 'Não sei, quero ver outra opção!', trigger: '19' },
          ],
        },
        {
          id: 'valor33',
          message: 'O investimento é de: R$3.600 ou 1 + 11 parcelas de: R$ 300,00. Material didádito anual: R$ 360,00 (até 12x no cartão de crédito).',
          trigger: 'schedule33',
        },
        //HORARIO DO CURSO LET IOT MAKER DA IDADE 7 - 10 DO INTERESSE CRIAR E MONTAR COISAS
        {
          id: 'schedule33',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'SEGUNDA | 15:00 - 16:30', label: 'SEGUNDA | 15:00 - 16:30',
                trigger: () => {
                  this.setState({ schedule: 'SEGUNDA | 15:00 - 16:30' });
                  return '20'
                },
              },
              {
                value: 'QUINTA | 10:30 - 12:00', label: 'QUINTA | 10:30 - 12:00',
                trigger: () => {
                  this.setState({ schedule: 'QUINTA | 10:30 - 12:00' });
                  return '20'
                },
              },
              {
                value: 'SEXTA | 16:30 - 18:00', label: 'SEXTA | 16:30 - 18:00',
                trigger: () => {
                  this.setState({ schedule: 'SEXTA | 16:30 - 18:00' });
                  return '20'
                },
              },
              {
                value: 'Hmmm... quero ver outras opções', label: 'Hmmm... quero ver outras opções',
                trigger: () => {
                  this.setState({ schedule: 'Hmmm... quero ver outras opções' });
                  return '19'
                },
              },
            ],
        },

        //MENSAGEM DA EXPLICAÇÃO DO CURSO LET DRONE 1
        {
          id: '3300',
          message: 'Conhecer os conceitos básicos de Robótica. Conhecer os conceitos básicos da Ciência da Computação. Conhecer os conceitos básicos de pilotagem do Drone através de programação. Conhecer os conceitos básicos de eletrônica. Aprendera  programar  contribui para  o desenvolvimento do  raciocínio lógico e também  para  o aprendizado de outras matérias como ciências, matemática e inglês.',
          trigger: '3301',
        },
        {
          id: '3301',
          options: [
            { value: '1', label: 'Legal!! Quais são os dias e valores', trigger: 'valor34' },
            { value: '2', label: 'É este curso mesmo, quero matricular!', trigger: 'valor34' },
            { value: '3', label: 'Não sei, quero ver outra opção!', trigger: '19' },
          ],
        },
        {
          id: 'valor34',
          message: 'O investimento é de: R$3.600 ou 1 + 11 parcelas de: R$ 300,00. Material didádito anual: R$ 360,00 (até 12x no cartão de crédito).',
          trigger: 'schedule34',
        },
        //HORARIO DO CURSO LET DRONE 1 DA IDADE 7 - 10 DO INTERESSE CRIAR E MONTAR COISAS
        {
          id: 'schedule34',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'SEGUNDA | 10:30 - 12:00', label: 'SEGUNDA | 10:30 - 12:00',
                trigger: () => {
                  this.setState({ schedule: 'SEGUNDA | 10:30 - 12:00' });
                  return '20'
                },
              },
              {
                value: 'QUARTA | 15:00 - 16:30', label: 'QUARTA | 15:00 - 16:30',
                trigger: () => {
                  this.setState({ schedule: 'QUARTA | 15:00 - 16:30' });
                  return '20'
                },
              },
              {
                value: 'SÁBADO | 09:00 - 10:30', label: 'SÁBADO | 09:00 - 10:30',
                trigger: () => {
                  this.setState({ schedule: 'SÁBADO | 09:00 - 10:30' });
                  return '20'
                },
              },
              {
                value: 'Hmmm... quero ver outras opções', label: 'Hmmm... quero ver outras opções',
                trigger: () => {
                  this.setState({ schedule: 'Hmmm... quero ver outras opções' });
                  return '19'
                },
              },
            ],
        },

        //CURSOS DA IDADE 11 - 13 ANOS CRIAR E MONTAR COISAS   ||||||||||
        {
          id: '3030',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Hmmm... com base na idade e nas atividades de ${nameCrop}, ja sei o que pode ser legal!!!!?`
          },
          trigger: '3031',
        },
        {
          id: '3031',
          asMessage: true,
          component: (
            <Gifs img={gif3} />
          ),
          trigger: '3032',
        },
        {
          id: '3032',
          message: 'Clique em uma das opções abaixo!!',
          trigger: 'classroom31',
        },
        {
          id: 'classroom31',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'LET IOT Maker', label: 'LET IOT Maker',
                trigger: () => {
                  this.setState({ classroom: 'LET IOT Maker' });
                  return '3230'
                },
              },
              {
                value: 'LET Drone 1', label: 'LET Drone 1',
                trigger: () => {
                  this.setState({ classroom: 'LET Drone 1' });
                  return '3330'
                },
              },
              {
                value: 'Hmmm... quero voltar', label: 'Hmmm... quero voltar',
                trigger: () => {
                  this.setState({ classroom: 'Hmmm... quero voltar' });
                  return '19'
                },
              },
            ],
        },
        //MENSAGEM DA EXPLICAÇÃO DO CURSO LET IOT MAKER
        {
          id: '3230',
          message: 'Ao concluir este curso os alunos serão capazes de construir aplicações e dispositivos inteligentes que utilizem robótica e internet das coisas, com base no movimento DYI – Do it yourself (faça você mesmo). Os estudantes precisarão usar a criatividade para criar protótipos e soluções customizadas.',
          trigger: '3231',
        },
        {
          id: '3231',
          options: [
            { value: '1', label: 'Legal!! Quais são os dias e valores', trigger: 'valor35' },
            { value: '2', label: 'É este curso mesmo, quero matricular!', trigger: 'valor35' },
            { value: '3', label: 'Não sei, quero ver outra opção!', trigger: '19' },
          ],
        },
        {
          id: 'valor35',
          message: 'O investimento é de: R$3.600 ou 1 + 11 parcelas de: R$ 300,00. Material didádito anual: R$ 360,00 (até 12x no cartão de crédito).',
          trigger: 'schedule35',
        },
        //HORARIO DO CURSO LET IOT MAKER DA IDADE 11 - 13 DO INTERESSE CRIAR E MONTAR COISAS
        {
          id: 'schedule35',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'SEGUNDA | 15:00 - 16:30', label: 'SEGUNDA | 15:00 - 16:30',
                trigger: () => {
                  this.setState({ schedule: 'SEGUNDA | 15:00 - 16:30' });
                  return '20'
                },
              },
              {
                value: 'QUINTA | 10:30 - 12:00', label: 'QUINTA | 10:30 - 12:00',
                trigger: () => {
                  this.setState({ schedule: 'QUINTA | 10:30 - 12:00' });
                  return '20'
                },
              },
              {
                value: 'SEXTA | 16:30 - 18:00', label: 'SEXTA | 16:30 - 18:00',
                trigger: () => {
                  this.setState({ schedule: 'SEXTA | 16:30 - 18:00' });
                  return '20'
                },
              },
              {
                value: 'Hmmm... quero ver outras opções', label: 'Hmmm... quero ver outras opções',
                trigger: () => {
                  this.setState({ schedule: 'Hmmm... quero ver outras opções' });
                  return '19'
                },
              },
            ],
        },
        //MENSAGEM DA EXPLICAÇÃO DO CURSO LET DRONE 1
        {
          id: '3330',
          message: 'Conhecer os conceitos básicos de Robótica. Conhecer os conceitos básicos da Ciência da Computação. Conhecer os conceitos básicos de pilotagem do Drone através de programação. Conhecer os conceitos básicos de eletrônica. Aprendera  programar  contribui para  o desenvolvimento do  raciocínio lógico e também  para  o aprendizado de outras matérias como ciências, matemática e inglês',
          trigger: '3331',
        },
        {
          id: '3331',
          options: [
            { value: '1', label: 'Legal!! Quais são os dias e valores', trigger: 'valor36' },
            { value: '2', label: 'É este curso mesmo, quero matricular!', trigger: 'valor36' },
            { value: '3', label: 'Não sei, quero ver outra opção!', trigger: '19' },
          ],
        },
        {
          id: 'valor36',
          message: 'O investimento é de: R$3.600 ou 1 + 11 parcelas de: R$ 300,00. Material didádito anual: R$ 360,00 (até 12x no cartão de crédito).',
          trigger: 'schedule36',
        },
        //HORARIO DO CURSO LET DRONE 1 IDADE 11 - 13 DO INTERESSE CRIAR E MONTAR COISAS
        {
          id: 'schedule36',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'SEGUNDA | 10:30 - 12:00', label: 'SEGUNDA | 10:30 - 12:00',
                trigger: () => {
                  this.setState({ schedule: 'SEGUNDA | 10:30 - 12:00' });
                  return '20'
                },
              },
              {
                value: 'QUARTA | 10:30 - 12:00', label: 'QUARTA | 10:30 - 12:00',
                trigger: () => {
                  this.setState({ schedule: 'QUARTA | 10:30 - 12:00' });
                  return '20'
                },
              },
              {
                value: 'SÁBADO | 09:00 - 10:30', label: 'SÁBADO | 09:00 - 10:30',
                trigger: () => {
                  this.setState({ schedule: 'SÁBADO | 09:00 - 10:30' });
                  return '20'
                },
              },
              {
                value: 'Hmmm... quero ver outras opções', label: 'Hmmm... quero ver outras opções',
                trigger: () => {
                  this.setState({ schedule: 'Hmmm... quero ver outras opções' });
                  return '19'
                },
              },
            ],
        },
        //CURSOS DA IDADE 14 -17 ANOS CRIAR E MONTAR COISAS   ||||||||||
        {
          id: '3040',
          message: () => {

            let nameCrop = this.state.name.split(' ');
            nameCrop = nameCrop[0];
            console.log(nameCrop);

            return `Hmmm... com base na idade e nas atividades de ${nameCrop}, ja sei o que pode ser legal!!!!?`
          },
          trigger: '3041',
        },
        {
          id: '3041',
          asMessage: true,
          component: (
            <Gifs img={gif3} />
          ),
          trigger: '3042',
        },
        {
          id: '3042',
          message: 'Clique em uma das opções abaixo!!',
          trigger: 'classroom34',
        },
        {
          id: 'classroom34',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'LET Drone 1', label: 'LET Drone 1',
                trigger: () => {
                  this.setState({ classroom: 'LET Drone 1' });
                  return '3340'
                },
              },
              {
                value: 'Hmmm... quero voltar', label: 'Hmmm... quero voltar',
                trigger: () => {
                  this.setState({ classroom: 'Hmmm... quero voltar' });
                  return '19'
                },
              },
            ],
        },
        //MENSAGEM DA EXPLICAÇÃO DO CURSO
        {
          id: '3340',
          message: 'Texto de explicação do curso LET Drone 1',
          trigger: '3341',
        },
        {
          id: '3341',
          options: [
            { value: '1', label: 'Legal!! Quais são os dias e valores', trigger: 'valor37' },
            { value: '2', label: 'É este curso mesmo, quero matricular!', trigger: 'valor37' },
            { value: '3', label: 'Não sei, quero ver outra opção!', trigger: '19' },
          ],
        },
        {
          id: 'valor37',
          message: 'O investimento é de: R$3.600 ou 1 + 11 parcelas de: R$ 300,00. Material didádito anual: R$ 360,00 (até 12x no cartão de crédito).',
          trigger: 'schedule37',
        },
        //HORARIO DO CURSO LET DRONE 1 DA IDADE 14 - 17 DO INTERESSE CRIAR E MONTAR COISAS
        {
          id: 'schedule37',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: 'SEGUNDA | 10:30 - 12:00', label: 'SEGUNDA | 10:30 - 12:00',
                trigger: () => {
                  this.setState({ schedule: 'SEGUNDA | 10:30 - 12:00' });
                  return '20'
                },
              },
              {
                value: 'QUARTA | 15:00 - 16:30', label: 'QUARTA | 15:00 - 16:30',
                trigger: () => {
                  this.setState({ schedule: 'QUARTA | 15:00 - 16:30' });
                  return '20'
                },
              },
              {
                value: 'SÁBADO | 09:00 - 10:30', label: 'SÁBADO | 09:00 - 10:30',
                trigger: () => {
                  this.setState({ schedule: 'SÁBADO | 09:00 - 10:30' });
                  return '20'
                },
              },
              {
                value: 'Hmmm... quero ver outras opções', label: 'Hmmm... quero ver outras opções',
                trigger: () => {
                  this.setState({ schedule: 'Hmmm... quero ver outras opções' });
                  return '19'
                },
              },
            ],
        },

        {
          id: '300',
          asMessage: true,
          component: (
            <Gifs img={gif2} />
          ),
          trigger: '301',
        },
        {
          id: '301',
          message: 'Que Legal! Você sabia que já existem pesquisas que mostram os benefícios de jogar vídeo game? Pesquisadores constataram que jogos eletrônicos desenvolvem regiões do cérebro responsáveis pela navegação espacial, formação de memória, habilidades motoras e até mesmo planejamento estratégico!',
          trigger: 'freetime',
        },
        //MENSAGEM DO INTERESSE VRIAR 
        {
          id: '300',
          asMessage: true,
          component: (
            <Gifs img={gif2} />
          ),
          trigger: '301',
        },
        {
          id: '301',
          message: 'Que Legal! Você sabia que já existem pesquisas que mostram os benefícios de jogar vídeo game? Pesquisadores constataram que jogos eletrônicos desenvolvem regiões do cérebro responsáveis pela navegação espacial, formação de memória, habilidades motoras e até mesmo planejamento estratégico!',
          trigger: 'freetime',
        },

        //MENSAGENS PADRÃO APÓS ESCOLHAS ACIMA
        {
          id: '20',
          message: 'Ja estamos finalizando a sua pré-matricula',
          trigger: '21',
        },
        {
          id: '21',
          message: 'Me informe o SEU nome!',
          trigger: 'parentsName',
        },
        {
          id: 'parentsName',
          user: true,
          validator: (value) => {
            if (value === '') {
              return 'Digite um nome';
            }
            if (!scripts.validadeName(value)) {
              return 'Nome Invalido'
            }
            return true;
          },
          trigger: (e) => {
            console.log(e);
            this.setState({ parentsName: e.value });
            return '23'
          },
        },
        {
          id: '23',
          message: 'Certo, qual o seu e-mail para contato?',
          trigger: 'parentsEmail',
        },
        {
          id: 'parentsEmail',
          user: true,
          validator: (value) => {
            if (!scripts.validateEmail(value)) {
              return 'email invalido'
            }
            return true;
          },
          trigger: (e) => {
            console.log(e);
            this.setState({ parentsEmail: e.value });
            return '25'
          },
        },
        {
          id: '25',
          message: 'Certo, qual o seu telefone para contato?',
          trigger: 'parentsContact',
        },
        {
          id: 'parentsContact',
          user: true,
          validator: (value) => {
            if (isNaN(value)) {
              return 'Digite números sem espaços e (*/-)';
            }
            if ((value.length) < 8) {
              return 'Número inválido';
            } if ((value.length) > 12) {
              return 'Número inválido';
            }
            return true;
          },
          trigger: (e) => {
            console.log(e);
            this.setState({ parentsContact: e.value });
            return '27'
          },
        },
        {
          id: '27',
          message: 'Pronto, vc gostaria de alterar alguma coisa? Nome do estudante, Nome do responsável, Telefone para contato ou até mesmo o e-mail?',
          trigger: 'finalizarCadastro',
        },
        {
          id: '31',
          message: 'Certo, alterei o campo informado, gostaria de alterar mais alguma coisa?',
          trigger: 'finalizarCadastro',
        },
        {
          id: '30',
          message: 'Você gostaria de alterar alguma informação?',
          trigger: 'update-fields',
        },
        {
          id: 'message-update-fields',
          message: 'Certo, escolha um dos campos abaixo para alterar!!',
          trigger: 'update-fields',
        },
        {
          id: 'update-fields',
          options: [
            { value: 'nome', label: 'Nome do Aluno', trigger: 'message-update-name' },
            { value: 'Responsável', label: 'Nome do Responsável', trigger: 'message-update-parentsName' },
            { value: 'Email', label: 'Email', trigger: 'message-update-parentsEmail' },
            { value: 'Telefone', label: 'Telefone', trigger: 'message-update-parentsContact' },
          ],
        },
        {
          id: 'message-update-name',
          message: 'escreva sua alteração',
          trigger: 'update-name',
        },
        {
          id: 'update-name',
          update: 'name',
          trigger: (e) => {
            console.log(e);
            this.setState({ name: e.value });
            return '27'
          }
        },
        {
          id: 'message-update-parentsName',
          message: 'escreva o novo nome',
          trigger: 'update-parentsName',
        },
        {
          id: 'update-parentsName',
          update: 'parentsName',
          trigger: (e) => {
            console.log(e);
            this.setState({ parentsName: e.value });
            return '27'
          }
        },
        {
          id: 'message-update-parentsEmail',
          message: 'escreva sua o novo e-mail',
          trigger: 'update-parentsEmail',
        },
        {
          id: 'update-parentsEmail',
          update: 'parentsEmail',
          trigger: (e) => {
            console.log(e);
            this.setState({ parentsEmail: e.value });
            return '27'
          },
        },
        {
          id: 'message-update-parentsContact',
          message: 'escreva o novo numero para contato',
          trigger: 'update-parentsContact',
        },
        {
          id: 'update-parentsContact',
          update: 'parentsContact',
          trigger: (e) => {
            console.log(e);
            this.setState({ parentsContact: e.value });
            return '27'
          },
        },
        {
          id: 'finalizarCadastro',
          options: [
            { value: 'Não', label: 'Não', trigger: () => {
              this.sendToApi();
              return 'mensagem-final' }
            },
            {
              value: 'Sim, Gostaria!', label: 'Sim, Gostaria!', trigger: 'message-update-fields'
              
            },
          ],
        },
        {
          id: 'mensagem-final',
          message: () => {

            let nameCrop = this.state.name;
            console.log(nameCrop);
            return `Pronto! A reserva  de ${nameCrop} está confirmada!            
                    Em breve nosso time de atendimento entrará em contato para informar os detalhes do pagamento e outras informações.
                    Qualquer dúvida, você pode entrar em contato com a gente pelo nosso telefone: (79)9 9999-9999.`
          },
          trigger: 'mensagem-final1',
        },
        {
          id: 'mensagem-final1',
          message: `Mas se preferir, pode entrar em contato conosco através de nosso Whatsapp clicando na imagem abaixo.`,
          trigger: 'end-message',
        },
        {
          id: 'end-message',
          asMessage: true,
          component: (
            <Links link={img2} path={"https://wa.me/5579988125929?text=Ol%C3%A1!%20Realizei%20a%20pr%C3%A9-matr%C3%ADcula%20e%20tenho%20interesse%20em%20saber%20mais%20sobre%20os%20cursos%20da%20Happy%20Code%20Aracaju"} />
          ),
          end: true,
        },
      ]
    }

  }

  componentDidMount() {
    document.title = "Happy Code Aracaju";
    console.log(this.props);
    const { name, freetime, age, classroom, schedule, parentsName, parentsEmail, parentsContact } = this.state.steps;
    this.setState({ name, freetime, age, classroom, schedule, parentsName, parentsEmail, parentsContact });

  }

  sendToApi = async () => {
    const userData = {
      name: this.state.name,
      freetime: this.state.freetime,
      age: this.state.age,
      class: this.state.classroom,
      schedule: this.state.schedule,
      parentsName: this.state.parentsName,
      parentsEmail: this.state.parentsEmail,
      parentsContact: this.state.parentsContact,
    }
    console.log('userData', userData);

    await Axios.post('https://us-central1-bot-happy-code.cloudfunctions.net/api/pre-matricula', userData)
      .then((res) => {
        console.log('Pré-matrícula efetuada com sucesso.');
      })
      .catch((error) => {
        console.log(error);
      })
  }


  render() {
    const theme = {
      background: '#f5f8fb',
      fontFamily: 'Helvetica Neue',
      headerBgColor: '#7FD38F',
      headerFontColor: 'yellow',
      headerFontSize: '20px',
      botBubbleColor: '#895CF2',
      botFontColor: '#fff',
      userBubbleColor: '#fff',
      userFontColor: '#4a4a4a',
    };

    return (
      <>

        <div style={{ width: 'auto', height: '100vh', display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          <ThemeProvider theme={theme}>
            <StyledChatbot style={{ textAlign: "left" }} placeholder="Escreva aqui..." steps={this.state.steps} botAvatar={img1} headerTitle={'Happy Code Aracaju'}/>
          </ThemeProvider>
        </div>

        <NavLink to="/login">Listagem de Pré-Matriculas</NavLink>

      </>

    );
  }
}

export const StyledChatbot = styled(ChatBot)`
  h2{
    text-align: center !important;
    color: #fff;
  }

  .dtHNed{
    margin-left:40px;
  }

  .drOyuD{
    margin-left:40px;
  }
  /* .rsc-ts-bubble{
    margin-left: 0;
  } */
`;
