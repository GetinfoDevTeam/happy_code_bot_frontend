import React, { Component } from 'react';

export default class Chatbot extends Component {
  constructor(props) {
    super(props);

    this.state = {

      steps: [

        //Exemplo de mensagem simples
        {
          id: '',
          message: '',
          trigger: '',
        },

        //Exemplo de mensagem com componentes dentro da mensagem
        {
          id: '',
          asMessage: true,
          component: (
            <InserirComponente />
          ),
          trigger: '',
        },

        //Exemplo de mensagem com componentes fora da mensagem
        {
          id: '',
          component: (
            <InserirComponente />
          ),
          trigger: '',
        },

        //Exemplo de opções
        {
          id: '',
          placeholder: 'escolha uma opção...',
          options:
            [
              {
                value: '', label: '',
                trigger: () => {
                  this.setState({ id: '' });
                  return ''
                },
              },
            ],
        },    
    }
  }
}

render() {
  const theme = {
    background: '#f5f8fb',
    fontFamily: 'Helvetica Neue',
    headerBgColor: '#7FD38F',
    headerFontColor: '#fff',
    headerFontSize: '15px',
    botBubbleColor: '#895CF2',
    botFontColor: '#fff',
    userBubbleColor: '#fff',
    userFontColor: '#4a4a4a',
  };

  return (
    <>
      <ThemeProvider theme={theme}>
        <ChatBot style={{ textAlign: "left" }} placeholder="Escreva aqui..." steps={this.state.steps} botAvatar={img} />
      </ThemeProvider>

      <NavLink to="/pre-matriculas">Listagem de Pré-Matriculas</NavLink>
    </>
  );
}
}