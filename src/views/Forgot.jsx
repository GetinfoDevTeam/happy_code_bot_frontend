import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import SimpleReactValidator from "simple-react-validator";
import firebase from "../firebase";
import withConsumer from "../components/AppContext/withConsumer";
import styled from 'styled-components';

import { Button, Row, Col, Layout } from 'antd';
import Input from '../components/Input';
import logo1 from '../assets/images/happycodesite.png';

const { Content } = Layout;

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      answer: "",
      isLoadingButton: false
    };

    this.validator = new SimpleReactValidator({
      messages: {
        default: "Campo obrigatório"
      }
    });
  }

  componentDidMount() {
    document.title = "Resetar senha  - Happy Code Aracaju";
  }

  handleFormChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
    this.setState({ answer: "" });
  };

  handleSubmit = async e => {
    e.preventDefault();

    this.setState({ isLoadingButton: true });
    this.setState({ answer: "" });

    if (!this.validator.allValid()) {
      this.setState({
        answer: [
          <i className="material-icons" key="">
            error_outline
          </i>,
          "Erros de validações nos campos."
        ]
      });
      this.setState({ isLoadingButton: false });
      this.validator.showMessages();
      this.forceUpdate();
      return;
    }

    firebase
      .auth()
      .sendPasswordResetEmail(this.state.email)
      .then(() => {
        this.setState({
          answer: [
            <i className="material-icons" key="">
              check_circle_outline
            </i>,
            "Se o e-mail existir em nosso sistema, será enviada uma solicitação de nova senha."
          ]
        });
        this.setState({ isLoadingButton: false });
      })
      .catch(() => {
        this.setState({
          answer: [
            <i className="material-icons" key="">
              check_circle_outline
            </i>,
            "Se o e-mail existir em nosso sistema, será enviada uma solicitação de nova senha."
          ]
        });
        this.setState({ isLoadingButton: false });
      });
  };

  render() {

    return (
      <div id="login-view">
        <StyledContent>
          <Row>
            <Col span={24}>
              <form onSubmit={this.handleSubmit}>
                <div class='logo' />
                <StyledInput
                  label="Login"
                  name="email"
                  type="email"
                  placeholder="e-mail"
                  value={this.state.email}
                  onChange={this.handleFormChange}
                />
                <span>
                  {this.validator.message(
                    "email",
                    this.state.email,
                    "required"
                  )}
                </span>


                <div className="answer margin-input">
                  {this.state.answer}
                </div>

                <div className="field margin-other btn">
                  <StyledButton
                    htmlType="submit"
                    type="primary"
                    loading={this.state.isLoadingButton}
                  >
                    Recuperar
                  </StyledButton>
                  <div className="back-login">
                    <NavLink to="/login">voltar</NavLink>
                  </div>
                </div>

                {/* <div className="field margin-other forgot">
                      <NavLink to="/login/redefinir-senha">
                        Esqueceu sua senha?
                      </NavLink>
                    </div> */}
              </form>
            </Col>
          </Row>
        </StyledContent>
      </div>
    );
  }
}

export default withConsumer(ForgotPassword);

const StyledContent = styled(Content)`
  background: #d9fcdf;
  display: flex;
  height: 100vh;
  justify-content: center; 
  align-items: center;

  form {
    color: #895BF2;
    width: 350px;
    height: 300px;
    background: #fff;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
    padding: 30px;
  }

  form label {
    font-style: italic;
    font-weight: bold
  }

  .srv-validation-message {
    color: red;
  }
  
  .logo{
      width: 50px;
      height: 50px;
      background-image: url(${logo1});
      background-repeat: no-repeat;
      position: relative;
      display: block !important;
      margin: auto;
    }  

    .back-login{
      position: relative;
      text-align: center;
      display: block !important;
      margin: auto;
    }
`;

const StyledInput = styled(Input)`
  margin-bottom: 10px !important;
`;

const StyledButton = styled(Button)`
  display: block !important;
  /* margin: auto !important; */
  margin: 20px auto !important;
  background: #895BF2 !important;
  border-color: #895BF2 !important;
`;