import React from "react";
import Routes from "./routes";
import withProvider from "./components/AppContext/withProvider";
import './App.css';

const App = () => (
  <div>
    <Routes />
  </div>
);

export default withProvider(App);