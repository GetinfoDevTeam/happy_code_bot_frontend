import React from "react";
import { Pagination as PaginationAntd } from 'antd';
import styled from 'styled-components';


const Pagination = props => (
  <div>
    {props.totalResults > props.limit ? (
      <StyledPagination
        total={props.totalResults}
        showTotal={(total, range) => `${range[0]}-${range[1]} de ${total} itens`}
        pageSize={props.limit}
        defaultCurrent={props.currentPage}
        onChange={(page) => {
          props.goToPage(page)
        }}
      />
    ) : null}
  </div>
);


const StyledPagination = styled(PaginationAntd)`

    li.ant-pagination-item-active{
      color: #7e77f8 !important;
      border-color: #7e77f8 !important;
    }

    li.ant-pagination-item:hover{
      color: #7e77f8 !important;
      border-color: #7e77f8 !important;
    }

    a{
      color: #7e77f8 !important
    }

    a:hover{
      color: #7e77f8 !important
    }
  

`;
export default Pagination;
