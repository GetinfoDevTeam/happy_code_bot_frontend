import React from 'react';
import Axios from 'axios';

export default class PersonList extends React.Component {
    state ={
        teste: []
    }

    componentDidMount() {
        Axios.get(`https://us-central1-bot-happy-code.cloudfunctions.net/api/pre-matricula`)
            .then(res => {
                console.log('eaeee', res.data.result);
                this.setState({ preMatriculaList: res.data.result });
            })
    }

    render() {
        const { teste } = this.state;
        return (
            <ul key={teste.id}>
            {this.state.teste.map(teste => <li>{teste.title}</li>)}    
            </ul>
        )
    }
}