import React from 'react';


const Gifs = props => {
    
    return (
        <div>
            <img style={{ width: '100%' }} src={props.img} alt=""/>
        </div>
    );
  }
export default Gifs;
