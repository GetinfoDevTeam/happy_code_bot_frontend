import React, { Component } from 'react';
import styled from 'styled-components';

class Table extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tds: ''
    };
  }

  incrementTd = (item, thead) => {
    let arr = [];
    var count = 0;

    for(var key in item) {
      arr.push(<td key={item.id + count}>{item[key]}</td>);
      count++;
    }

    return arr;
  }

  render() {
    return (
      <table>
        <thead>
          <tr>
            {this.props.thead.map((item, i) => (
              <th key={i}>{item}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {this.props.tbody.map((item, i) => (
            <tr key={item.id}>
              {this.incrementTd(item)}
            </tr>
          ))}
        </tbody>
      </table>
    )
  }
}

export default Table;