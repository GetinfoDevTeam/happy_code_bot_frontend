import React, { Component } from 'react';
 import { Input as InputAntd } from 'antd';
 
 export default class Input extends Component {
  
     render() {
         const { type, label } = this.props;
 
         return (
             <>
                 <label>{label}</label>
                 {type === 'password' ?
                     <InputAntd.Password {...this.props} />
                 :
                     <InputAntd {...this.props} />
                 }
             </>
         );
                }
            }