import React from 'react';
// import { NavLink } from 'react-router-dom';



const Links = props => {

    return (
        <div>
            <a href={props.path} target='_blank' rel='noopener noreferrer'>
                <img style={{ width: '100%' }} src={props.link} alt=""/>
            </a>
            
        </div>
    );
}
export default Links;
