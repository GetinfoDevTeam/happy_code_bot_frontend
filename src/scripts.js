class Scripts {
  handleCpf = cpf => {
    const cpf_without_hyphens = cpf.replace(/-/g, "");
    const handledCpf = cpf_without_hyphens.replace(/\./g, "");
    return handledCpf;
  };

  validateEmail = email => {
    if ((/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email))) {
      console.log('email verdadeiro' + email)
      return true
    }
    console.log(' email falso' + email)
    return false;
  }

  validadeName = name => {
    if ((/^[A-Z-a-z][a-zA-Z][^1-9#&<>"~,.;:!?/$+^%{[\]º}=_()\\|@*?]{1,100}$/.test(name))) {
      console.log('Nome Verdadeiro ' + name)
      return true
    }
    console.log('Nome Falso' + name)
    return false;
  }

  capitalizeFirstLetter = firstLetter => {
    if (typeof firstLetter !== 'string') return ''
  return firstLetter.charAt(0).toUpperCase() + firstLetter.slice(1)
  } 

  prettyPhone = phone => {
    let almostPrettyPhone;
    let prettyPhone;
    let noOpen = phone.replace("(", "");
    let noClose = noOpen.replace(")", "");
    almostPrettyPhone = noClose.replace("-", "");
    // prettyPhone = prettyPhone.replace("(", "");
    switch (almostPrettyPhone.length) {
      case 11:
        prettyPhone =
          "(" +
          almostPrettyPhone.slice(0, 2) +
          ") " +
          almostPrettyPhone.slice(2, 3) +
          " " +
          almostPrettyPhone.slice(3, 7) +
          "-" +
          almostPrettyPhone.slice(7, 11);
        break;
      case 10:
        prettyPhone =
          "(" +
          almostPrettyPhone.slice(0, 2) +
          ") " +
          almostPrettyPhone.slice(2, 6) +
          "-" +
          almostPrettyPhone.slice(6, 10);
        break;
      case 9:
        prettyPhone =
          almostPrettyPhone.slice(0, 5) + "-" + almostPrettyPhone.slice(5, 10);
        break;
      case 8:
        prettyPhone =
          almostPrettyPhone.slice(0, 4) + "-" + almostPrettyPhone.slice(4, 9);
        break;

      default:
        break;
    }
    return prettyPhone;
  };

  handlePhone = phone => {
    const phoneWithoutParenthesysOpen = phone.replace(/\(/g, "");
    const phoneWithoutParenthesysClose = phoneWithoutParenthesysOpen.replace(
      /\)/g,
      ""
    );
    const phoneWithoutHyphens = phoneWithoutParenthesysClose.replace(/-/g, "");
    const phoneWithoutUnderscore = phoneWithoutHyphens.replace(/_/g, "");
    const handledPhone = phoneWithoutUnderscore.replace(/ /g, "");
    return handledPhone;
  };

  dateToIso = date => {
    const dia = date.slice(0, 2);
    const mes = date.slice(3, 5);
    const ano = date.slice(6, 10);
    const data_iso = ano + "-" + mes + "-" + dia;
    return data_iso;
  };

  isoToDate = iso => {
    const ano = iso.slice(0, 4);
    const mes = iso.slice(5, 7);
    const dia = iso.slice(8, 10);
    const data = dia + "/" + mes + "/" + ano;
    return data;
  };

  handleDecimal = value => {
    const formatter = new Intl.NumberFormat("pt-BR", {
      style: "currency",
      currency: "BRL",
      minimumFractionDigits: 2
    });

    return formatter.format(value);
  };

  handleDecimalBlur = value => {
    let clear = value.replace("R$ ", "lala");
    return clear;
  };

  encode = string => {
    let encoded = encodeURIComponent(string);
    return encoded;
  };

  clearEmptyProperties = obj => {
    let propNames = Object.getOwnPropertyNames(obj);
    for (let i = 0; i < propNames.length; i++) {
      let propName = propNames[i];
      if (
        obj[propName] === null ||
        obj[propName] === undefined ||
        obj[propName] === ""
      ) {
        delete obj[propName];
      }
    }
    return obj;
  };

  formatMoney = str => {
    const value = parseFloat(str);

    const formatValue = value.toLocaleString("pt-BR", {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });

    const returnValue = "R$ " + formatValue.toString();

    return returnValue;
  };

  formatMoneyWithoutCurrency = str => {
    const value = parseFloat(str);

    const formatValue = value.toLocaleString("pt-BR", {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });

    const returnValue = formatValue.toString();

    return returnValue;
  };
}

export default new Scripts();