import React from "react";
import { Switch, Route, BrowserRouter as Router, Redirect } from "react-router-dom";
import withConsumer from "./components/AppContext/withConsumer";
import Login from './views/Login'
import Logout from './views/Logout'
import Chatbot from './views/Chatbot'
import PreMatriculas from "./views/PreMatriculas";
import Forgot from "./views/Forgot";



const PrivateRoute = withConsumer(
  ({ component: Component, isAdmin, ...rest }) => (
    <Route
      {...rest}
      render={props => {
        if (isAdmin && rest.userScope && !rest.userScope.admin) {
          return (
            <Redirect to={{ pathname: "/", state: { from: props.location } }} />
          );
        }

        return rest.userAuth ? (
          <Component {...props} {...rest} />
        ) : (
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        );
      }}
    />
  )
);



const Routes = () => (
  <Router>
    <main>
      <Switch>
        <Route exact path="/" component={Chatbot} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/forgot" component={Forgot} />
        <Route path='/whatsapp' component={(Chatbot) => {
          const url = "https://api.whatsapp.com/sendphone=5579988125929&text=Olá!%20Tenho%20interesse%20em%20saber%20mais%20sobre%20Os%20Cursos%20da%20Happy%20Code%20Aracaju"
          return window.location.href = `${url}`;
        }} />
        <PrivateRoute exact path="/pre-matriculas" component={PreMatriculas} />
        <PrivateRoute exact path="/logout" component={Logout} />
      </Switch>
    </main>
  </Router>
);

export default Routes;
